<?php

namespace Drupal\Tests\view_mode_crop\Functional;

use AssertGD\GDAssertTrait;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\responsive_image\Entity\ResponsiveImageStyle;
use Drupal\Tests\BrowserTestBase;
use Drupal\view_mode_crop\ViewModeCropData;

/**
 * Tests the image ViewModeCropResponsiveImageFormatter class.
 *
 * @group view_mode_crop
 */
class ViewModeCropResponsiveImageFormatterTest extends BrowserTestBase {

  use GDAssertTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stable';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'file',
    'field',
    'image',
    'view_mode_crop',
    'entity_test',
    'field_test',
    'responsive_image_test_module',
  ];

  /**
   * The entity type.
   *
   * @var string
   */
  protected $entityType;

  /**
   * The entity bundle.
   *
   * @var string
   */
  protected $bundle;

  /**
   * The entity view display.
   *
   * @var \Drupal\Core\Entity\Display\EntityViewDisplayInterface
   */
  protected $display;

  /**
   * Public PNG file.
   *
   * @var \Drupal\file\FileInterface
   */
  protected $publicPngFile;

  /**
   * The image factory.
   *
   * @var \Drupal\Core\Image\ImageFactory
   */
  protected $imageFactory;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->entityType = 'entity_test';
    $this->bundle = $this->entityType;

    FieldStorageConfig::create([
      'entity_type' => $this->entityType,
      'field_name' => 'field_public_image',
      'type' => 'image',
      'cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
      'settings' => [
        'uri_scheme' => 'public',
      ],
    ])->save();
    FieldConfig::create([
      'entity_type' => $this->entityType,
      'field_name' => 'field_public_image',
      'bundle' => $this->bundle,
      'settings' => [
        'file_extensions' => 'png',
      ],
    ])->save();

    $responsive_image_style = ResponsiveImageStyle::create([
      'id' => 'style_one',
      'label' => 'Style One',
      'breakpoint_group' => 'responsive_image_test_module',
      'fallback_image_style' => 'large',
    ]);
    $responsive_image_style
      ->addImageStyleMapping('responsive_image_test_module.mobile', '1x', [
        'image_mapping_type' => 'image_style',
        'image_mapping' => 'thumbnail',
      ])
      ->addImageStyleMapping('responsive_image_test_module.narrow', '1x', [
        'image_mapping_type' => 'image_style',
        'image_mapping' => 'medium',
      ])
      // Test the normal output of mapping to an image style.
      ->addImageStyleMapping('responsive_image_test_module.wide', '1x', [
        'image_mapping_type' => 'image_style',
        'image_mapping' => 'large',
      ])
      ->save();

    $this->display = \Drupal::service('entity_display.repository')
      ->getViewDisplay($this->entityType, $this->bundle)
      ->setComponent('field_public_image', [
        'type' => 'view_mode_crop_responsive_image',
        'label' => 'hidden',
        'settings' => [
          'responsive_image_style' => 'style_one',
        ],
      ]);
    $this->display->save();

    /** @var \Drupal\file\FileRepositoryInterface $file_repository */
    $file_repository = $this->container->get('file.repository');
    $this->publicPngFile = $file_repository->writeData(file_get_contents(__DIR__ . '/assets/test.png'), 'public://test.png');

    $this->imageFactory = $this->container->get('image.factory');

    $this->renderer = $this->container->get('renderer');
  }

  /**
   * Test if the correct uri and dimensions are set.
   */
  public function testUriAndCropDimensionsSet(): void {
    $crop_data1 = [
      'default' => new ViewModeCropData('default', 'default', 100, 101, 102, 103),
    ];
    $crop_data2 = [
      'default' => new ViewModeCropData('default', 'default', 1, 2, 3, 4),
    ];
    $entity = EntityTest::create([
      'name' => $this->randomMachineName(),
      'field_public_image' => [
        0 => [
          'target_id' => $this->publicPngFile->id(),
          'view_mode_crop' => json_encode($crop_data1),
        ],
        1 => [
          'target_id' => $this->publicPngFile->id(),
          'view_mode_crop' => json_encode($crop_data2),
        ],
      ],
    ]);
    $entity->save();

    $build = $this->display->build($entity);

    $field_public_image_0 = $this->renderer->renderPlain($build['field_public_image'][0]);
    $field_public_image_1 = $this->renderer->renderPlain($build['field_public_image'][1]);

    $this->assertEquals(1, preg_match('/files\/styles\/thumbnail\/crop-public\/entity_test\/' . $entity->id() . '\/field_public_image\/0\/default\/test.png.*\s1x" media="\(min-width: 0px\)/', $field_public_image_0));
    $this->assertEquals(1, preg_match('/files\/styles\/medium\/crop-public\/entity_test\/' . $entity->id() . '\/field_public_image\/0\/default\/test.png.*\s1x" media="\(min-width: 560px\)/', $field_public_image_0));
    $this->assertEquals(1, preg_match('/files\/styles\/large\/crop-public\/entity_test\/' . $entity->id() . '\/field_public_image\/0\/default\/test.png.*\s1x" media="\(min-width: 851px\)/', $field_public_image_0));

    $this->assertEquals(1, preg_match('/files\/styles\/thumbnail\/crop-public\/entity_test\/' . $entity->id() . '\/field_public_image\/1\/default\/test.png.*\s1x" media="\(min-width: 0px\)/', $field_public_image_1));
    $this->assertEquals(1, preg_match('/files\/styles\/medium\/crop-public\/entity_test\/' . $entity->id() . '\/field_public_image\/1\/default\/test.png.*\s1x" media="\(min-width: 560px\)/', $field_public_image_1));
    $this->assertEquals(1, preg_match('/files\/styles\/large\/crop-public\/entity_test\/' . $entity->id() . '\/field_public_image\/1\/default\/test.png.*\s1x" media="\(min-width: 851px\)/', $field_public_image_1));
  }

}
