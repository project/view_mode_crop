<?php

namespace Drupal\Tests\view_mode_crop\Functional;

use AssertGD\GDAssertTrait;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\BrowserTestBase;
use Drupal\view_mode_crop\ViewModeCropData;

/**
 * Tests the ViewModeCropImageFactory class.
 *
 * @group view_mode_crop
 */
class ViewModeCropImageFactoryTest extends BrowserTestBase {

  use GDAssertTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stable';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'file',
    'field',
    'image',
    'view_mode_crop',
    'entity_test',
    'field_test',
  ];

  /**
   * The entity type.
   *
   * @var string
   */
  protected $entityType;

  /**
   * The entity bundle.
   *
   * @var string
   */
  protected $bundle;

  /**
   * The entity view display.
   *
   * @var \Drupal\Core\Entity\Display\EntityViewDisplayInterface
   */
  protected $display;

  /**
   * Public PNG file.
   *
   * @var \Drupal\file\FileInterface
   */
  protected $publicPngFile;

  /**
   * Private PNG file.
   *
   * @var \Drupal\file\FileInterface
   */
  protected $privatePngFile;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The image factory.
   *
   * @var \Drupal\Core\Image\ImageFactory
   */
  protected $imageFactory;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->entityType = 'entity_test';
    $this->bundle = $this->entityType;

    FieldStorageConfig::create([
      'entity_type' => $this->entityType,
      'field_name' => 'field_public_image',
      'type' => 'image',
      'cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
      'settings' => [
        'uri_scheme' => 'public',
      ],
    ])->save();
    FieldConfig::create([
      'entity_type' => $this->entityType,
      'field_name' => 'field_public_image',
      'bundle' => $this->bundle,
      'settings' => [
        'file_extensions' => 'png',
      ],
    ])->save();

    FieldStorageConfig::create([
      'entity_type' => $this->entityType,
      'field_name' => 'field_private_image',
      'type' => 'image',
      'cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
      'settings' => [
        'uri_scheme' => 'private',
      ],
    ])->save();
    FieldConfig::create([
      'entity_type' => $this->entityType,
      'field_name' => 'field_private_image',
      'bundle' => $this->bundle,
      'settings' => [
        'file_extensions' => 'png',
      ],
    ])->save();

    $this->display = \Drupal::service('entity_display.repository')
      ->getViewDisplay($this->entityType, $this->bundle)
      ->setComponent('field_public_image', [
        'type' => 'view_mode_crop_image_url',
        'label' => 'hidden',
        'settings' => [
          'image_style' => '',
        ],
      ])
      ->setComponent('field_private_image', [
        'type' => 'view_mode_crop_image_url',
        'label' => 'hidden',
        'settings' => [
          'image_style' => '',
        ],
      ]);
    $this->display->save();

    /** @var \Drupal\file\FileRepositoryInterface $file_repository */
    $file_repository = $this->container->get('file.repository');
    $this->publicPngFile = $file_repository->writeData(file_get_contents(__DIR__ . '/assets/test.png'), 'public://test.png');
    $this->privatePngFile = $file_repository->writeData(file_get_contents(__DIR__ . '/assets/test.png'), 'private://test.png');

    $this->imageFactory = $this->container->get('image.factory');
  }

  /**
   * Check if the image.factory opens the original file and re-crops it.
   */
  public function testLoadImageCropScheme(): void {
    $crop_data = [
      'default' => new ViewModeCropData('default', 'default', 10, 10, 100, 100),
      'other' => new ViewModeCropData($this->display->id(), $this->display->id(), 12, 34, 56, 78),
    ];

    $entity = EntityTest::create([
      'name' => $this->randomMachineName(),
      'field_public_image' => [
        0 => [
          'target_id' => $this->publicPngFile->id(),
          'view_mode_crop' => json_encode($crop_data),
        ],
      ],
      'field_private_image' => [
        0 => [
          'target_id' => $this->privatePngFile->id(),
          'view_mode_crop' => json_encode($crop_data),
        ],
      ],
    ]);
    $entity->save();

    $this->assertTrue(file_exists('crop-public://entity_test/' . $entity->id() . '/field_public_image/0/default/test.png'));

    $expected_image = $this->imageFactory->get($this->publicPngFile->getFileUri());
    $expected_image->crop($crop_data['default']->x, $crop_data['default']->y, $crop_data['default']->w, $crop_data['default']->h);
    $expected_image->save('public://crop-test.png');
    $this->assertSimilarGD('public://crop-test.png', 'crop-public://entity_test/' . $entity->id() . '/field_public_image/0/default/test.png');

    unlink($this->publicPngFile->getFileUri());
    copy(__DIR__ . '/assets/other-test.png', $this->publicPngFile->getFileUri());

    $expected_image = $this->imageFactory->get(__DIR__ . '/assets/other-test.png');
    $expected_image->crop($crop_data['default']->x, $crop_data['default']->y, $crop_data['default']->w, $crop_data['default']->h);
    $expected_image->save('public://crop-test2.png');

    $crop_public_image = $this->imageFactory->get('crop-public://entity_test/' . $entity->id() . '/field_public_image/0/default/test.png');
    $crop_public_image->save('public://crop-public-image.png');
    $this->assertSimilarGD('public://crop-test2.png', 'public://crop-public-image.png');

  }

}
