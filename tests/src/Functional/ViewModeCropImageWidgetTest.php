<?php

namespace Drupal\Tests\view_mode_crop\Functional;

use Drupal\node\Entity\Node;
use Drupal\Tests\image\Functional\ImageFieldTestBase;
use Drupal\view_mode_crop\ViewModeCropData;

/**
 * Tests the image ViewModeCropImageWidget field widget.
 *
 * @group view_mode_crop
 */
class ViewModeCropImageWidgetTest extends ImageFieldTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stable';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'view_mode_crop',
  ];

  /**
   * The entity view display.
   *
   * @var \Drupal\Core\Entity\Display\EntityViewDisplayInterface
   */
  protected $display;

  /**
   * Public png file.
   *
   * @var \Drupal\file\FileInterface
   */
  protected $publicPngFile;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->display = \Drupal::service('entity_display.repository')
      ->getViewDisplay('node', 'article');

    /** @var \Drupal\file\FileRepositoryInterface $file_repository */
    $file_repository = $this->container->get('file.repository');
    $this->publicPngFile = $file_repository->writeData(file_get_contents(__DIR__ . '/assets/test.png'), 'public://test.png');

    $this->createImageField('field_public_image', 'article', [], [], [], [], 'Image test on [site:name]');
    $this->database = $this->container->get('database');
  }

  /**
   * Test the image widget without cropping.
   *
   * When no display mode of the related entity has a view mode crop formatter,
   * cropping should not be available when editing the entity.
   */
  public function testCroppingNotAvailableWithoutViewModeCropFormatter(): void {
    $this->display->setComponent('field_public_image', [
      'type' => 'image_url',
      'label' => 'hidden',
    ]);
    $this->display->save();

    // When adding a node, the image field is empty and the crop open button
    // should not be visible.
    $this->drupalGet('node/add/article');
    $this->assertSession()
      ->elementExists('xpath', '//div[contains(@class, "field--widget-image-image")]');
    $this->assertSession()
      ->elementNotExists('xpath', '//input[contains(@class, "view-mode-crop-open-button")]');

    // When editing a node with an empty image field, the crop open button
    // should not be visible.
    $this->drupalGet('node/add/article');
    $this->submitForm([
      'title[0][value]' => $this->randomMachineName(),
    ], 'Save');
    preg_match('/node\/([0-9]+)/', $this->getUrl(), $matches);
    $this->drupalGet('node/' . $matches[1] . '/edit');
    $this->assertSession()
      ->elementExists('xpath', '//div[contains(@class, "field--widget-image-image")]');
    $this->assertSession()
      ->elementNotExists('xpath', '//input[contains(@class, "view-mode-crop-open-button")]');

    // When editing a node with a filled image field, the crop open button
    // should not be visible.
    $this->drupalGet('node/add/article');
    $this->submitForm([
      'title[0][value]' => $this->randomMachineName(),
      'files[field_public_image_0]' => \Drupal::service('file_system')
        ->realpath($this->publicPngFile->getFileUri()),
    ], 'Save');
    $this->submitForm(['field_public_image[0][alt]' => 'test'], 'Save');
    preg_match('/node\/([0-9]+)/', $this->getUrl(), $matches);
    $this->drupalGet('node/' . $matches[1] . '/edit');
    $this->assertSession()
      ->elementExists('xpath', '//div[contains(@class, "field--widget-image-image")]');
    $this->assertSession()
      ->elementExists('xpath', '//img[contains(@data-drupal-selector, "edit-field-public-image-0-preview")]');
    $this->assertSession()
      ->elementNotExists('xpath', '//input[contains(@class, "view-mode-crop-open-button")]');
  }

  /**
   * Test the image widget with cropping.
   *
   * When at least one display mode of the related entity has a view mode crop
   * formatter, cropping should be available when an image is selected.
   */
  public function testCroppingAvailableWithViewModeCropFormatter(): void {
    $this->display->setComponent('field_public_image', [
      'type' => 'view_mode_crop_image_url',
      'label' => 'hidden',
    ]);
    $this->display->save();

    // When adding a node, the image field is empty and the crop open button
    // should not be visible.
    $this->drupalGet('node/add/article');
    $this->assertSession()
      ->elementExists('xpath', '//div[contains(@class, "field--widget-image-image")]');
    $this->assertSession()
      ->elementNotExists('xpath', '//input[contains(@class, "view-mode-crop-open-button")]');

    // When editing a node with an empty image field, the crop open button
    // should not be visible.
    $this->drupalGet('node/add/article');
    $this->submitForm([
      'title[0][value]' => $this->randomMachineName(),
    ], 'Save');
    preg_match('/node\/([0-9]+)/', $this->getUrl(), $matches);
    $this->drupalGet('node/' . $matches[1] . '/edit');
    $this->assertSession()
      ->elementExists('xpath', '//div[contains(@class, "field--widget-image-image")]');
    $this->assertSession()
      ->elementNotExists('xpath', '//input[contains(@class, "view-mode-crop-open-button")]');

    // When editing a node with a filled image field, the crop open button
    // should be visible.
    $this->drupalGet('node/add/article');
    $this->submitForm([
      'title[0][value]' => $this->randomMachineName(),
      'files[field_public_image_0]' => \Drupal::service('file_system')
        ->realpath($this->publicPngFile->getFileUri()),
    ], 'Save');
    $this->submitForm(['field_public_image[0][alt]' => 'test'], 'Save');
    preg_match('/node\/([0-9]+)/', $this->getUrl(), $matches);
    $this->drupalGet('node/' . $matches[1] . '/edit');
    $this->assertSession()
      ->elementExists('xpath', '//div[contains(@class, "field--widget-image-image")]');
    $this->assertSession()
      ->elementExists('xpath', '//img[contains(@data-drupal-selector, "edit-field-public-image-0-preview")]');
    $this->assertSession()
      ->elementExists('xpath', '//input[contains(@class, "view-mode-crop-open-button")]');
  }

  /**
   * Test that cropping data is saved into the database.
   */
  public function testSaveCroppingData(): void {
    $this->display->setComponent('field_public_image', [
      'type' => 'view_mode_crop_image_url',
      'label' => 'hidden',
    ]);
    $this->display->save();

    // When no cropping data is saved, no cropping data should appear in the
    // database.
    $this->database->truncate('view_mode_crop');
    $this->drupalGet('node/add/article');
    $this->submitForm([
      'title[0][value]' => $this->randomMachineName(),
      'files[field_public_image_0]' => \Drupal::service('file_system')
        ->realpath($this->publicPngFile->getFileUri()),
    ], 'Save');
    $this->submitForm([
      'field_public_image[0][alt]' => 'test',
      'field_public_image[0][view_mode_crop_state]' => '',
    ], 'Save');
    preg_match('/node\/([0-9]+)/', $this->getUrl(), $matches);
    $this->drupalGet('node/' . $matches[1] . '/edit');

    $view_mode_crop_records = $this->database->select('view_mode_crop', 'vmc')
      ->fields('vmc', [
        'entity_type',
        'entity_id',
        'field_name',
        'delta',
        'data',
      ])
      ->condition('entity_type', 'node')
      ->condition('entity_id', $matches[1])
      ->execute()
      ->fetchAll();
    $this->assertCount(0, $view_mode_crop_records);

    // Cropping data should appear in the database.
    $this->database->truncate('view_mode_crop');
    $this->drupalGet('node/add/article');
    $this->submitForm([
      'title[0][value]' => $this->randomMachineName(),
      'files[field_public_image_0]' => \Drupal::service('file_system')
        ->realpath($this->publicPngFile->getFileUri()),
    ], 'Save');

    $view_mode_crop_state = [
      'entityTypeId' => 'node',
      'id' => NULL,
      'fieldName' => 'field_public_image',
      'delta' => 0,
      'imageUrl' => '',
      'data' => [
        'default' => new ViewModeCropData('default', 'default label', 1, 2, 3, 4),
        'other' => new ViewModeCropData('other', 'other label', 5, 6, 7, 8),
      ],
    ];
    $this->submitForm([
      'field_public_image[0][alt]' => 'test',
      'field_public_image[0][view_mode_crop_state]' => json_encode($view_mode_crop_state),
    ], 'Save');
    preg_match('/node\/([0-9]+)/', $this->getUrl(), $matches);
    $this->drupalGet('node/' . $matches[1] . '/edit');

    $node = Node::load($matches[1]);

    $view_mode_crop_records = $this->database->select('view_mode_crop', 'vmc')
      ->fields('vmc', [
        'entity_type',
        'entity_id',
        'field_name',
        'delta',
        'data',
      ])
      ->condition('entity_type', 'node')
      ->condition('entity_id', $node->uuid())
      ->execute()
      ->fetchAll();
    $this->assertCount(1, $view_mode_crop_records);
    $this->assertEquals('field_public_image', $view_mode_crop_records[0]->field_name);
    $this->assertEquals(0, $view_mode_crop_records[0]->delta);
    $this->assertEquals(json_encode($view_mode_crop_state['data']), $view_mode_crop_records[0]->data);
  }

}
