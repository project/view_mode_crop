<?php

namespace Drupal\Tests\view_mode_crop\Functional;

use AssertGD\GDAssertTrait;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\BrowserTestBase;
use Drupal\view_mode_crop\ViewModeCropData;

/**
 * Tests the image ViewModeCropImageFormatter class.
 *
 * @group view_mode_crop
 */
class ViewModeCropImageFormatterTest extends BrowserTestBase {

  use GDAssertTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stable';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'file',
    'field',
    'image',
    'view_mode_crop',
    'entity_test',
    'field_test',
  ];

  /**
   * The entity type.
   *
   * @var string
   */
  protected $entityType;

  /**
   * The entity bundle.
   *
   * @var string
   */
  protected $bundle;

  /**
   * The entity view display.
   *
   * @var \Drupal\Core\Entity\Display\EntityViewDisplayInterface
   */
  protected $display;

  /**
   * Public PNG file.
   *
   * @var \Drupal\file\FileInterface
   */
  protected $publicPngFile;

  /**
   * The image factory.
   *
   * @var \Drupal\Core\Image\ImageFactory
   */
  protected $imageFactory;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->entityType = 'entity_test';
    $this->bundle = $this->entityType;

    FieldStorageConfig::create([
      'entity_type' => $this->entityType,
      'field_name' => 'field_public_image',
      'type' => 'image',
      'cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
      'settings' => [
        'uri_scheme' => 'public',
      ],
    ])->save();
    FieldConfig::create([
      'entity_type' => $this->entityType,
      'field_name' => 'field_public_image',
      'bundle' => $this->bundle,
      'settings' => [
        'file_extensions' => 'png',
      ],
    ])->save();

    $this->display = \Drupal::service('entity_display.repository')
      ->getViewDisplay($this->entityType, $this->bundle)
      ->setComponent('field_public_image', [
        'type' => 'view_mode_crop_image',
        'label' => 'hidden',
        'settings' => [
          'image_style' => '',
        ],
      ]);
    $this->display->save();

    /** @var \Drupal\file\FileRepositoryInterface $file_repository */
    $file_repository = $this->container->get('file.repository');
    $this->publicPngFile = $file_repository->writeData(file_get_contents(__DIR__ . '/assets/test.png'), 'public://test.png');

    $this->imageFactory = $this->container->get('image.factory');

    $this->renderer = $this->container->get('renderer');
  }

  /**
   * Test if the correct uri and dimensions are set.
   */
  public function testUriAndCropDimensionsSet(): void {
    $crop_data1 = [
      'default' => new ViewModeCropData('default', 'default', 100, 101, 102, 103),
    ];
    $crop_data2 = [
      'default' => new ViewModeCropData('default', 'default', 1, 2, 3, 4),
    ];
    $entity = EntityTest::create([
      'name' => $this->randomMachineName(),
      'field_public_image' => [
        0 => [
          'target_id' => $this->publicPngFile->id(),
          'view_mode_crop' => json_encode($crop_data1),
        ],
        1 => [
          'target_id' => $this->publicPngFile->id(),
          'view_mode_crop' => json_encode($crop_data2),
        ],
      ],
    ]);
    $entity->save();

    $build = $this->display->build($entity);

    $field_public_image_0 = $this->renderer->renderPlain($build['field_public_image'][0]);
    $field_public_image_1 = $this->renderer->renderPlain($build['field_public_image'][1]);

    $this->assertStringContainsString('files/crop/entity_test/' . $entity->id() . '/field_public_image/0/default/test.png', $field_public_image_0);
    $this->assertStringContainsString('width="102"', $field_public_image_0);
    $this->assertStringContainsString('height="103"', $field_public_image_0);
    $this->assertStringContainsString('files/crop/entity_test/' . $entity->id() . '/field_public_image/1/default/test.png', $field_public_image_1);
    $this->assertStringContainsString('width="3"', $field_public_image_1);
    $this->assertStringContainsString('height="4"', $field_public_image_1);
  }

}
