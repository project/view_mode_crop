<?php

namespace Drupal\Tests\view_mode_crop\Functional;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\image\Entity\ImageStyle;
use Drupal\media\Entity\Media;
use Drupal\node\Entity\Node;
use Drupal\Tests\media\Functional\MediaFunctionalTestBase;
use Drupal\view_mode_crop\ViewModeCropData;

/**
 * Tests the image ViewModeCropMediaLibraryWidget field widget.
 *
 * @group view_mode_crop
 * @requires module webp
 */
class CropImageHelperTest extends MediaFunctionalTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stable';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'media_library',
    'node',
    'media',
    'text',
    'view_mode_crop',
    'webp',
  ];

  /**
   * PNG image.
   *
   * @var \Drupal\file\FileInterface
   */
  protected $publicPngFile;

  /**
   * Other PNG image.
   *
   * @var \Drupal\file\FileInterface
   */
  protected $otherPublicPngFile;

  /**
   * Test media.
   *
   * @var \Drupal\media\MediaInterface
   */
  protected $media;

  /**
   * Other test media.
   *
   * @var \Drupal\media\MediaInterface
   */
  protected $otherMedia;

  /**
   * Media type image view display.
   *
   * @var \Drupal\Core\Entity\Display\EntityViewDisplayInterface
   */
  protected $mediaTypeImageViewDisplay;

  /**
   * The node entity view display.
   *
   * @var \Drupal\Core\Entity\Display\EntityViewDisplayInterface
   */
  protected $display;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->drupalCreateContentType(['type' => 'article']);
    $user = $this->drupalCreateUser([
      'access media overview',
      'create article content',
      'edit any article content',
      'delete any article content',
    ]);
    $media_type_image = $this->createMediaType('image', [
      'id' => 'image',
      'label' => 'Image',
    ]);

    /** @var \Drupal\file\FileRepositoryInterface $file_repository */
    $file_repository = $this->container->get('file.repository');
    $this->publicPngFile = $file_repository->writeData(file_get_contents(__DIR__ . '/assets/test.png'), 'public://test.png');
    $this->otherPublicPngFile = $file_repository->writeData(file_get_contents(__DIR__ . '/assets/test.png'), 'public://other-test.png');
    $this->media = Media::create([
      'bundle' => 'image',
      'name' => 'Public png image',
      'field_media_image' => [
        [
          'target_id' => $this->publicPngFile->id(),
          'alt' => 'default alt',
          'title' => 'default title',
        ],
      ],
    ]);
    $this->media->save();

    $this->otherMedia = Media::create([
      'bundle' => 'image',
      'name' => 'Other public png image',
      'field_media_image' => [
        [
          'target_id' => $this->otherPublicPngFile->id(),
          'alt' => 'default alt',
          'title' => 'default title',
        ],
      ],
    ]);
    $this->otherMedia->save();

    FieldStorageConfig::create([
      'field_name' => 'field_media',
      'entity_type' => 'node',
      'type' => 'entity_reference',
      'settings' => [
        'target_type' => 'media',
      ],
      'cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
    ])->save();

    $field_config = FieldConfig::create([
      'field_name' => 'field_media',
      'label' => 'field_media',
      'entity_type' => 'node',
      'bundle' => 'article',
      'required' => FALSE,
      'settings' => [
        'handler_settings' => [
          'target_bundles' => [
            $media_type_image->id() => $media_type_image->id(),
          ],
        ],
      ],
    ]);
    $field_config->save();

    FieldStorageConfig::create([
      'entity_type' => 'node',
      'field_name' => 'field_public_image',
      'type' => 'image',
      'cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
      'settings' => [
        'uri_scheme' => 'public',
      ],
    ])->save();
    FieldConfig::create([
      'entity_type' => 'node',
      'field_name' => 'field_public_image',
      'bundle' => 'article',
      'settings' => [
        'file_extensions' => 'png',
      ],
    ])->save();

    /**
     * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $display_repository
     */
    $display_repository = \Drupal::service('entity_display.repository');
    $display_repository->getFormDisplay('node', 'article')
      ->setComponent('field_media', [
        'type' => 'media_library_widget',
        'settings' => [],
      ])
      ->save();
    $this->display = $display_repository->getViewDisplay('node', 'article');

    $this->display->setComponent('field_media', [
      'type' => 'entity_reference_entity_view',
      'settings' => [
        'view_mode' => 'full',
      ],
    ])->setComponent('field_public_image', [
      'type' => 'view_mode_crop_image_url',
      'label' => 'hidden',
      'settings' => [
        'image_style' => '',
      ],
    ])->save();

    $this->mediaTypeImageViewDisplay = $display_repository->getViewDisplay('media', 'image', 'full');
    $this->mediaTypeImageViewDisplay->setComponent('field_media_image', [
      'type' => 'view_mode_crop_image_url',
    ])->save();

    $this->drupalLogin($user);

    $this->renderer = $this->container->get('renderer');
  }

  /**
   * TestGetUri.
   */
  public function testGetUri(): void {
    $crop_data = [
      'default' => new ViewModeCropData('default', 'default', 100, 100, 100, 100),
    ];
    $entity = Node::create([
      'title' => $this->randomMachineName(),
      'type' => 'article',
      'field_public_image' => [
        0 => [
          'target_id' => $this->publicPngFile->id(),
          'view_mode_crop' => json_encode($crop_data),
        ],
        1 => [
          'target_id' => $this->otherPublicPngFile->id(),
          'view_mode_crop' => json_encode($crop_data),
        ],
      ],
      'field_media' => [
        0 => [
          'target_id' => $this->media->id(),
          'view_mode_crop' => json_encode($crop_data),
        ],
        1 => [
          'target_id' => $this->otherMedia->id(),
          'view_mode_crop' => json_encode($crop_data),
        ],
      ],
    ]);
    $entity->save();

    $build = $this->display->build($entity);

    $this->assertStringContainsString('files/crop/node/' . $entity->id() . '/field_public_image/0/default/test.png', $this->renderer->renderPlain($build['field_public_image'][0]));
    $this->assertStringContainsString('files/crop/node/' . $entity->id() . '/field_public_image/1/default/other-test.png', $this->renderer->renderPlain($build['field_public_image'][1]));

    $this->assertStringContainsString('files/crop/node/' . $entity->id() . '/field_media/0/default/test.png', $this->renderer->renderPlain($build['field_media'][0]));
    $this->assertStringContainsString('files/crop/node/' . $entity->id() . '/field_media/1/default/other-test.png', $this->renderer->renderPlain($build['field_media'][1]));
  }

  /**
   * TestGetUriReferencingSameEntities.
   */
  public function testGetUriReferencingSameEntities(): void {
    $crop_data = [
      'default' => new ViewModeCropData('default', 'default', 100, 100, 100, 100),
    ];
    $entity = Node::create([
      'title' => $this->randomMachineName(),
      'type' => 'article',
      'field_public_image' => [
        0 => [
          'target_id' => $this->publicPngFile->id(),
          'view_mode_crop' => json_encode($crop_data),
        ],
        1 => [
          'target_id' => $this->publicPngFile->id(),
          'view_mode_crop' => json_encode($crop_data),
        ],
      ],
      'field_media' => [
        0 => [
          'target_id' => $this->media->id(),
          'view_mode_crop' => json_encode($crop_data),
        ],
        1 => [
          'target_id' => $this->media->id(),
          'view_mode_crop' => json_encode($crop_data),
        ],
      ],
    ]);
    $entity->save();

    $build = $this->display->build($entity);

    $rendered = $this->renderer->renderRoot($build);

    $this->assertStringContainsString('files/crop/node/' . $entity->id() . '/field_public_image/0/default/test.png', $rendered);
    $this->assertStringContainsString('files/crop/node/' . $entity->id() . '/field_public_image/1/default/test.png', $rendered);

    $this->assertStringContainsString('files/crop/node/' . $entity->id() . '/field_media/0/default/test.png', $rendered);
    $this->assertStringContainsString('files/crop/node/' . $entity->id() . '/field_media/1/default/test.png', $rendered);
  }

  /**
   * TestImageStyleDerivatesCleared.
   */
  public function testImageStyleDerivatesCleared(): void {
    $this->display->setComponent('field_public_image', [
      'type' => 'view_mode_crop_image',
      'label' => 'hidden',
      'settings' => [
        'image_style' => 'large',
      ],
    ])->save();

    $crop_data = [
      'default' => new ViewModeCropData('default', 'default', 100, 100, 100, 100),
    ];
    $entity = Node::create([
      'title' => $this->randomMachineName(),
      'type' => 'article',
      'field_public_image' => [
        0 => [
          'target_id' => $this->publicPngFile->id(),
          'view_mode_crop' => json_encode($crop_data),
        ],
      ],
    ]);
    $entity->save();

    $build = $this->display->build($entity);

    $rendered = $this->renderer->renderRoot($build);

    /**
     * @var \Drupal\image\ImageStyleInterface $image_style
     */
    $image_style = ImageStyle::load('large');

    /**
     * @var \Drupal\webp\Webp $webp
     */
    $webp = \Drupal::service('webp.webp');

    $crop_uri = 'crop-public://node/' . $entity->id() . '/field_public_image/0/default/test.png';
    $crop_uri_webp = $webp->getWebpSrcset($crop_uri);
    $image_style_uri = $image_style->buildUri($crop_uri);
    $image_style_uri_webp = $webp->getWebpSrcset($image_style_uri);
    $image_style->createDerivative($crop_uri, $image_style_uri);

    $this->assertStringContainsString('files/styles/large/crop-public/node/' . $entity->id() . '/field_public_image/0/default/test.png', $rendered);

    $this->assertTrue(file_exists($crop_uri));
    $this->assertTrue(file_exists($image_style_uri));
    $this->assertFalse(file_exists($crop_uri_webp));
    $this->assertFalse(file_exists($image_style_uri_webp));

    // Assume webp generated its derivates.
    copy($crop_uri, $crop_uri_webp);
    copy($image_style_uri, $image_style_uri_webp);

    $this->assertTrue(file_exists($crop_uri_webp));
    $this->assertTrue(file_exists($image_style_uri_webp));

    // Remove the cropped file.
    unlink($crop_uri);
    clearstatcache(TRUE, $crop_uri);

    // File_exists will recreate $crop_uri and should remove all derivates.
    $this->assertTrue(file_exists($crop_uri));
    $this->assertFalse(file_exists($image_style_uri));
    $this->assertFalse(file_exists($crop_uri_webp));
    $this->assertFalse(file_exists($image_style_uri_webp));
  }

}
