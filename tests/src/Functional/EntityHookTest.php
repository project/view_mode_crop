<?php

namespace Drupal\Tests\view_mode_crop\Functional;

use AssertGD\GDAssertTrait;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\BrowserTestBase;
use Drupal\view_mode_crop\ViewModeCropData;

/**
 * Tests the entity update/hooks.
 *
 * @group view_mode_crop
 */
class EntityHookTest extends BrowserTestBase {

  use GDAssertTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stable';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'file',
    'field',
    'image',
    'view_mode_crop',
    'entity_test',
    'field_test',
  ];

  /**
   * The entity type.
   *
   * @var string
   */
  protected $entityType;

  /**
   * The entity bundle.
   *
   * @var string
   */
  protected $bundle;

  /**
   * The entity view display.
   *
   * @var \Drupal\Core\Entity\Display\EntityViewDisplayInterface
   */
  protected $display;

  /**
   * Public PNG file.
   *
   * @var \Drupal\file\FileInterface
   */
  protected $publicPngFile;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->entityType = 'entity_test';
    $this->bundle = $this->entityType;

    FieldStorageConfig::create([
      'entity_type' => $this->entityType,
      'field_name' => 'field_public_image',
      'type' => 'image',
      'cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
      'settings' => [
        'uri_scheme' => 'public',
      ],
    ])->save();
    FieldConfig::create([
      'entity_type' => $this->entityType,
      'field_name' => 'field_public_image',
      'bundle' => $this->bundle,
      'settings' => [
        'file_extensions' => 'png',
      ],
    ])->save();

    $this->display = \Drupal::service('entity_display.repository')
      ->getViewDisplay($this->entityType, $this->bundle)
      ->setComponent('field_public_image', [
        'type' => 'view_mode_crop_image',
        'label' => 'hidden',
        'settings' => [
          'image_style' => 'medium',
        ],
      ]);
    $this->display->save();

    /** @var \Drupal\file\FileRepositoryInterface $file_repository */
    $file_repository = $this->container->get('file.repository');
    $this->publicPngFile = $file_repository->writeData(file_get_contents(__DIR__ . '/assets/test.png'), 'public://test.png');
  }

  /**
   * Test if an entity update recreates the cropped files.
   */
  public function testEntityUpdateShouldDeleteCroppedImages(): void {
    $crop_data = [
      'default' => new ViewModeCropData('default', 'default', 100, 100, 100, 100),
    ];

    $entity = EntityTest::create([
      'name' => $this->randomMachineName(),
      'field_public_image' => [
        0 => [
          'target_id' => $this->publicPngFile->id(),
          'view_mode_crop' => json_encode($crop_data),
        ],
      ],
    ]);
    $entity->save();

    $this->display->build($entity);

    $uri = 'crop-public://entity_test/' . $entity->id() . '/field_public_image/0/default/test.png';
    $this->assertFileExists($uri);

    file_put_contents($uri, '1234');
    clearstatcache($uri, TRUE);
    $size = filesize($uri);

    $entity->save();

    $this->assertNotEquals($size, filesize($uri));
  }

  /**
   * Test that cropped images are deleted when an entity is deleted.
   */
  public function testEntityDeleteShouldDeleteCroppedImagesDirectory(): void {
    $crop_data = [
      'default' => new ViewModeCropData('default', 'default', 100, 100, 100, 100),
    ];

    $entity = EntityTest::create([
      'name' => $this->randomMachineName(),
      'field_public_image' => [
        0 => [
          'target_id' => $this->publicPngFile->id(),
          'view_mode_crop' => json_encode($crop_data),
        ],
      ],
    ]);
    $entity->save();

    $this->display->build($entity);

    $this->assertFileExists('crop-public://entity_test/' . $entity->id() . '/field_public_image/0/default/test.png');
    $entity->delete();
    $this->assertDirectoryDoesNotExist('crop-public://entity_test/' . $entity->id());
  }

}
