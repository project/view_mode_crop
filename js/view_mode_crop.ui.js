(function (Drupal, $) {
  function ViewModeCrop(view_mode, view_mode_crop_ui) {
    var image,
      cropper,
      self = this,
      initial_cropper_data = {
        x: view_mode_crop_ui.state.data[view_mode.id].x,
        y: view_mode_crop_ui.state.data[view_mode.id].y,
        width: view_mode_crop_ui.state.data[view_mode.id].w,
        height: view_mode_crop_ui.state.data[view_mode.id].h,
      };

    this.element = document.createElement('div');
    this.element.classList.add('view-mode-crop-view-mode-image-container');

    this.show = function () {
      this.element.style.display = '';

      cropper = new Cropper(image, {
        rotatable: false,
        scalable: false,
        zoomable: false,
        data: {
          x: view_mode_crop_ui.state.data[view_mode.id].x,
          y: view_mode_crop_ui.state.data[view_mode.id].y,
          width: view_mode_crop_ui.state.data[view_mode.id].w,
          height: view_mode_crop_ui.state.data[view_mode.id].h,
        },
        autoCrop: true,
        viewMode: 1,
        autoCropArea: 1,
        crop: function (event) {
          view_mode_crop_ui.state.data[view_mode.id].x = Math.round(
            event.detail.x
          );
          view_mode_crop_ui.state.data[view_mode.id].y = Math.round(
            event.detail.y
          );
          view_mode_crop_ui.state.data[view_mode.id].w = Math.round(
            event.detail.width
          );
          view_mode_crop_ui.state.data[view_mode.id].h = Math.round(
            event.detail.height
          );

          if (isNaN(view_mode_crop_ui.state.data[view_mode.id].x)) {
            view_mode_crop_ui.state.data[view_mode.id].x = null;
          }
          if (isNaN(view_mode_crop_ui.state.data[view_mode.id].y)) {
            view_mode_crop_ui.state.data[view_mode.id].y = null;
          }
          if (isNaN(view_mode_crop_ui.state.data[view_mode.id].w)) {
            view_mode_crop_ui.state.data[view_mode.id].w = null;
          }
          if (isNaN(view_mode_crop_ui.state.data[view_mode.id].h)) {
            view_mode_crop_ui.state.data[view_mode.id].h = null;
          }
        },
        ready: function () {
          window.dispatchEvent(new Event('resize'));
        },
      });
    };

    this.hide = function () {
      if (cropper) {
        cropper.crop();
        cropper.destroy();
      }
      this.element.style.display = 'none';
    };

    this.selectAll = function () {
      cropper.reset();
    }

    this.reset = function () {
      cropper.setData(initial_cropper_data);
    }

    function init() {
      image = document.createElement('img');
      image.src = view_mode_crop_ui.state.imageUrl;
      self.element.appendChild(image);
    }

    init();
  }

  function ViewModeCropUi(element, dialog) {
    var view_modes = [],
      view_mode,
      i,
      state_storage,
      self = this,
      menu = element.querySelector('.view-mode-crop-menu'),
      li,
      content = element.querySelector('.view-mode-crop-content'),
      current_view_mode_id,
      self = this;

    function setActiveViewMode(view_mode_id) {
      var i = view_modes.length;

      while (i--) {
        if (view_modes[i].id == view_mode_id) {
          view_modes[i].menu_anchor.classList.add('active');
          view_modes[i].view_mode_crop.show();
        }
        else {
          view_modes[i].view_mode_crop.hide();
          view_modes[i].menu_anchor.classList.remove('active');
        }
      }
      current_view_mode_id = view_mode_id;
    }

    function menuAnchorOnClick(e) {
      e.preventDefault();

      setActiveViewMode(this.getAttribute('data-view-mode-id'));

      return false;
    }

    this.entityTypeId = element.getAttribute('data-entity-type-id');
    this.entityId = element.getAttribute('data-entity-id');
    this.fieldName = element.getAttribute('data-field-name');
    this.delta = element.getAttribute('data-delta');
    this.element = element;
    this.triggeringElementName = element.getAttribute('data-triggering-element-name');

    state_storage = document.body.querySelector('textarea[data-view-mode-crop-triggering-element-name="' + this.triggeringElementName + '"]');

    this.state = JSON.parse(state_storage.value);

    for (i in this.state.data) {
      if (!this.state.data.hasOwnProperty(i)) {
        continue;
      }
      view_mode = {
        id: i,
        label: this.state.data[i].label,
        menu_anchor: document.createElement('a'),
        view_mode_crop: undefined,
      };

      view_mode.view_mode_crop = new ViewModeCrop(view_mode, this);

      li = document.createElement('li');
      view_mode.menu_anchor.appendChild(
        document.createTextNode(this.state.data[i].label)
      );
      view_mode.menu_anchor.className = 'view-mode-crop-menu__link';

      li.appendChild(view_mode.menu_anchor);
      menu.appendChild(li);
      view_mode.menu_anchor.setAttribute('data-view-mode-id', i);
      view_mode.menu_anchor.addEventListener('click', menuAnchorOnClick);

      content.appendChild(view_mode.view_mode_crop.element);
      view_modes[view_modes.length] = view_mode;
    }

    function getViewMode(view_mode_id) {
      var i = view_modes.length;
      while (i--) {
        if (view_modes[i].id === view_mode_id) {
          return view_modes[i];
        }
      }
      return undefined;
    }

    function buttonSelectAllOnClick(e) {
      e.preventDefault();
      var view_mode = getViewMode(current_view_mode_id);
      if (view_mode) {
        view_mode.view_mode_crop.selectAll();
      }
    }

    function buttonSelectionResetOnClick(e) {
      e.preventDefault();
      var view_mode = getViewMode(current_view_mode_id);
      if (view_mode) {
        view_mode.view_mode_crop.reset();
      }
    }

    function buttonSaveOnClick(e) {
      e.preventDefault();
      state_storage.value = JSON.stringify(self.state);

      dialog.close();
    }

    function buttonCancelOnClick() {
      dialog.close();
    }

    document
      .getElementById('view-mode-crop-' + this.entityTypeId + '-' + (this.entityId || '') + '-' + this.fieldName + '-' + this.delta + '-button-all')
      .addEventListener('click', buttonSelectAllOnClick);
    document
      .getElementById('view-mode-crop-' + this.entityTypeId + '-' + (this.entityId || '') + '-' + this.fieldName + '-' + this.delta + '-button-reset')
      .addEventListener('click', buttonSelectionResetOnClick);
    document
      .getElementById('view-mode-crop-' + this.entityTypeId + '-' + (this.entityId || '') + '-' + this.fieldName + '-' + this.delta + '-button-save-close')
      .addEventListener('click', buttonSaveOnClick);
    document
      .getElementById('view-mode-crop-' + this.entityTypeId + '-' + (this.entityId || '') + '-' + this.fieldName + '-' + this.delta + '-button-cancel')
      .addEventListener('click', buttonCancelOnClick);

    setActiveViewMode(view_modes[0].id);
  }

  Drupal.behaviors.view_mode_crop_ui = {
    attach: function (context, settings) {

      $(window).on('dialog:aftercreate', function (event, dialog, element) {
        var wrapper = element[0].querySelector('.view-mode-crop-wrapper');
        if (wrapper && !wrapper.view_mode_crop_ui) {
          wrapper.view_mode_crop_ui = new ViewModeCropUi(wrapper, dialog);
        }
      });
    }
  };

})(Drupal, jQuery);
