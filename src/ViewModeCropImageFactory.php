<?php

namespace Drupal\view_mode_crop;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Image\ImageFactory;
use Drupal\Core\StreamWrapper\StreamWrapperManager;
use Drupal\media\MediaInterface;
use Drupal\view_mode_crop\Plugin\Field\FieldType\ViewModeCropEntityReferenceItem;
use Drupal\view_mode_crop\Plugin\Field\FieldType\ViewModeCropImageItem;

/**
 * Image factory service.
 */
class ViewModeCropImageFactory extends ImageFactory {

  /**
   * {@inheritDoc}
   */
  public function get($source = NULL, $toolkit_id = NULL) {
    $scheme = StreamWrapperManager::getScheme($source);

    if (stripos($scheme, 'crop-') === 0) {
      // When opening a cropped file, open the original and re-crop it
      // to prevent loss of quality that would happen when re-saving a
      // lossy image format.
      $parsed = CropImageHelper::parsePathOrUri(StreamWrapperManager::getTarget($source));

      if ($parsed !== NULL) {
        $image = parent::get($parsed['scheme'] . '://' . $parsed['file'], $toolkit_id);

        $referring_item = NULL;
        $entity = \Drupal::entityTypeManager()
          ->getStorage($parsed['entity_type_id'])
          ->load($parsed['entity_id']);

        if (!$entity instanceof ContentEntityInterface) {
          return parent::get($source, $toolkit_id);
        }

        $item = $entity->get($parsed['field_name'])->get($parsed['delta']);

        if ($entity instanceof MediaInterface) {
          $referring_item = $entity->_referringItem;
        }

        if (
          $referring_item instanceof ViewModeCropEntityReferenceItem
        ) {
          $crop_data = $referring_item->getViewModeCropData();
        }
        elseif ($item instanceof ViewModeCropImageItem || $item instanceof ViewModeCropEntityReferenceItem) {
          $crop_data = $item->getViewModeCropData();
        }
        else {
          throw new \RuntimeException('Cannot handle ' . get_class($item));
        }

        if (empty($crop_data)) {
          return parent::get($source, $toolkit_id);
        }

        CropImageHelper::cropImage($image, $parsed['view_mode'], $crop_data);

        return $image;
      }
    }
    return parent::get($source, $toolkit_id);
  }

}
