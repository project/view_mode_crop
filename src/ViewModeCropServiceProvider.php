<?php

namespace Drupal\view_mode_crop;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Service provider which overrides image.factory.
 */
class ViewModeCropServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritDoc}
   */
  public function alter(ContainerBuilder $container) {
    $definition = $container->getDefinition('image.factory');
    $definition->setClass(ViewModeCropImageFactory::class);
  }

}
