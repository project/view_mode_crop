<?php

namespace Drupal\view_mode_crop\Routing;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;

/**
 * Supplies the crop routes.
 */
class CropRoutes implements ContainerInjectionInterface {

  /**
   * The stream wrapper manager service.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  protected $streamWrapperManager;

  /**
   * Constructs a new ImageStyleRoutes object.
   *
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $stream_wrapper_manager
   *   The stream wrapper manager service.
   */
  public function __construct(StreamWrapperManagerInterface $stream_wrapper_manager) {
    $this->streamWrapperManager = $stream_wrapper_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('stream_wrapper_manager')
    );
  }

  /**
   * Returns an array of route objects.
   *
   * @return \Symfony\Component\Routing\Route[]
   *   An array of route objects.
   */
  public function routes() {
    $routes = [];
    // Generate image derivatives of publicly available files. If clean URLs are
    // disabled image derivatives will always be served through the menu system.
    // If clean URLs are enabled and the image derivative already exists, PHP
    // will be bypassed.
    /**
     * @var \Drupal\Core\StreamWrapper\PublicStream $public_stream_wrapper
     */
    $public_stream_wrapper = $this->streamWrapperManager->getViaScheme('public');
    $public_directory_path = $public_stream_wrapper->getDirectoryPath();

    $routes['view_mode_crop.public'] = new Route(
      '/' . $public_directory_path . '/crop/{entity_type_id}/{entity_id}/{field_name}/{delta}/{view_mode}',
      [
        '_controller' => 'Drupal\view_mode_crop\Controller\DownloadCropController::download',
        '_disable_route_normalizer' => 'TRUE',
      ],
      [
        '_access' => 'TRUE',
      ]
    );

    return $routes;
  }

}
