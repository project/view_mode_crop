<?php

namespace Drupal\view_mode_crop\StreamWrapper;

use Drupal\Core\Site\Settings;
use Drupal\Core\StreamWrapper\StreamWrapperInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;

/**
 * Crop-private:// stream wrapper.
 */
class CropPrivateStreamWrapper extends CropStreamWrapper {

  /**
   * {@inheritdoc}
   */
  public function getDirectoryPath() {
    return static::basePath();
  }

  /**
   * {@inheritdoc}
   */
  public static function getType() {
    return StreamWrapperInterface::LOCAL;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return new TranslatableMarkup('Crop private files');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return new TranslatableMarkup('Crop private local files served by the webserver.');
  }

  /**
   * {@inheritdoc}
   */
  public function getExternalUrl() {
    $target = str_replace('\\', '/', $this->getTarget());
    $path = 'crop/' . $target;
    $this->createDerivate('crop-private://' . $target);

    return Url::fromRoute('system.private_file_download', ['filepath' => $path], [
      'absolute' => TRUE,
      'path_processing' => FALSE,
    ])->toString();
  }

  /**
   * Returns the base path for crop-private://.
   *
   * @param string $site_path
   *   (optional) The site.path service parameter, which is typically the path
   *   to sites/ in a Drupal installation. This allows you to inject the site
   *   path using services from the caller. If omitted, this method will use
   *   the
   *   global service container or the kernel's default behavior to determine
   *   the site path.
   *
   * @return string
   *   The base path for crop-private://.
   */
  public static function basePath($site_path = NULL) {
    return Settings::get('file_private_path') . '/crop';
  }

}
