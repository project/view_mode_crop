<?php

namespace Drupal\view_mode_crop\PathProcessor;

use Drupal\view_mode_crop\CropImageHelper;
use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Path processor to resolve downloading of cropped images.
 */
class DownloadPathProcessor implements InboundPathProcessorInterface {

  /**
   * The stream wrapper manager service.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  protected $streamWrapperManager;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $stream_wrapper_manager
   *   The stream wrapper manager service.
   */
  public function __construct(StreamWrapperManagerInterface $stream_wrapper_manager) {
    $this->streamWrapperManager = $stream_wrapper_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function processInbound($path, Request $request) {
    $parsed = CropImageHelper::parsePathOrUri($path);

    if ($parsed === NULL || empty($parsed['entity_type_id'])) {
      return $path;
    }

    if ($parsed['scheme'] === 'public') {
      // Set the file as query parameter.
      $request->query->set('file', $parsed['file']);
      return $parsed['path_prefix'] . $parsed['entity_type_id'] . '/' . $parsed['entity_id'] . '/' . $parsed['field_name'] . '/' . $parsed['delta'] . '/' . $parsed['view_mode'];
    }
    elseif ($parsed['scheme'] === 'private') {
      // Make sure the derivate image exists.
      $crop_private_uri = 'crop-private://' . $parsed['entity_type_id'] . '/' . $parsed['entity_id'] . '/' . $parsed['field_name'] . '/' . $parsed['delta'] . '/' . $parsed['view_mode'] . '/' . $parsed['file'];
      if (file_exists($crop_private_uri) === FALSE) {
        throw new \RuntimeException('Crop image was not created');
      }
      return $path;
    }

    return $path;
  }

}
