<?php

namespace Drupal\view_mode_crop;

/**
 * Container for the cropper ui state.
 */
class ViewModeCropState {

  /**
   * The entity type id.
   *
   * @var string
   */
  public $entityTypeId;

  /**
   * The entity id.
   *
   * @var string|null
   */
  public $id;

  /**
   * The field name.
   *
   * @var string
   */
  public $fieldName;

  /**
   * The field delta.
   *
   * @var int
   */
  public $delta;

  /**
   * The url to the image we're cropping.
   *
   * @var string
   */
  public $imageUrl;

  /**
   * The crop data.
   *
   * @var \Drupal\view_mode_crop\ViewModeCropData[]
   */
  public $data;

  /**
   * Constructor.
   *
   * @param string $entityTypeId
   *   The entity type id.
   * @param string|null $id
   *   The entity id.
   * @param string $fieldName
   *   The field name.
   * @param int $delta
   *   The field delta.
   * @param string $imageUrl
   *   The url to the image we're cropping.
   * @param \Drupal\view_mode_crop\ViewModeCropData[] $data
   *   The crop data.
   */
  public function __construct(string $entityTypeId, $id, string $fieldName, int $delta, string $imageUrl, array $data) {
    $this->entityTypeId = $entityTypeId;
    $this->id = $id;
    $this->fieldName = $fieldName;
    $this->delta = $delta;
    $this->imageUrl = $imageUrl;
    $this->data = $data;
  }

  /**
   * Create an instance from a JSON string.
   *
   * @param string $json
   *   The JSON string.
   *
   * @return static
   *   The instance.
   */
  public static function createFromJsonString(string $json) {
    $json = json_decode($json, TRUE);
    $data = [];
    foreach ($json['data'] as $view_mode_id => $view_mode_data) {
      $data[$view_mode_id] = new ViewModeCropData(
        $view_mode_data['id'],
        $view_mode_data['label'],
        $view_mode_data['x'],
        $view_mode_data['y'],
        $view_mode_data['w'],
        $view_mode_data['h']
      );
    }

    return new static($json['entityTypeId'], $json['id'], $json['fieldName'], $json['delta'], $json['imageUrl'], $data);
  }

}
