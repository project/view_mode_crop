<?php

namespace Drupal\view_mode_crop\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\media\MediaInterface;
use Drupal\responsive_image\Plugin\Field\FieldFormatter\ResponsiveImageFormatter;

/**
 * Plugin for view_mode_crop responsive image formatter.
 *
 * @FieldFormatter(
 *   id = "view_mode_crop_responsive_image",
 *   label = @Translation("Responsive image (cropped)"),
 *   field_types = {
 *     "image",
 *   },
 *   quickedit = {
 *     "editor" = "image"
 *   },
 *   view_mode_crop_enabled = true
 * )
 */
class ViewModeCropResponsiveImageFormatter extends ResponsiveImageFormatter {

  use ViewModeCropImageFormatterTrait;

  /**
   * {@inheritDoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);

    /**
     * @var \Drupal\file\FileInterface[] $files
     */
    $files = $this->getEntitiesToView($items, $langcode);

    $items_entity = $items->getEntity();
    $referring_item = NULL;
    if ($items_entity instanceof MediaInterface) {
      $referring_item = $items_entity->_referringItem;
    }

    $field_name = $items->getFieldDefinition()->getName();

    foreach ($files as $delta => $file) {
      $elements[$delta]['#item']->uri = $file->getFileUri();
      if (isset($elements[$delta]['#item'])) {
        if (!empty($file->viewModeCropWidth) && !empty($file->viewModeCropHeight)) {
          // Update the dimensions which are saved in
          // ViewModeCropImageFormatterGetEntitiesToViewTrait::getEntitiesToView.
          $elements[$delta]['#item']->width = $file->viewModeCropWidth;
          $elements[$delta]['#item']->height = $file->viewModeCropHeight;
        }
      }

      foreach ($items->getEntity()->getCacheTags() as $tag) {
        $elements[$delta]['#cache']['tags'][] = $tag;
        $elements[$delta]['#cache']['tags'][] = $tag . ':' . $field_name;
        $elements[$delta]['#cache']['tags'][] = $tag . ':' . $field_name . ':' . $delta;
      }

      if ($referring_item) {
        foreach ($referring_item->getEntity()->getCacheTags() as $tag) {
          $elements[$delta]['#cache']['tags'][] = $tag;
          $elements[$delta]['#cache']['tags'][] = $tag . ':' . $field_name;
          $elements[$delta]['#cache']['tags'][] = $tag . ':' . $field_name . ':' . $delta;
        }
      }

      $elements[$delta]['#cache']['tags'][] = $items->getFieldDefinition()
        ->getName();
    }

    return $elements;

  }

}
