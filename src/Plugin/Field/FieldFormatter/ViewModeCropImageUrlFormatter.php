<?php

namespace Drupal\view_mode_crop\Plugin\Field\FieldFormatter;

use Drupal\image\Plugin\Field\FieldFormatter\ImageUrlFormatter;

/**
 * Plugin implementation of the 'view_mode_crop_image_url' formatter.
 *
 * @FieldFormatter(
 *   id = "view_mode_crop_image_url",
 *   label = @Translation("URL to image (cropped)"),
 *   field_types = {
 *     "image"
 *   },
 *   view_mode_crop_enabled = true
 * )
 */
class ViewModeCropImageUrlFormatter extends ImageUrlFormatter {

  use ViewModeCropImageFormatterTrait;

}
