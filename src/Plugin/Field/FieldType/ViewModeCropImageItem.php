<?php

namespace Drupal\view_mode_crop\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\image\Plugin\Field\FieldType\ImageItem;
use Drupal\view_mode_crop\CropImageHelper;
use Drupal\view_mode_crop\ViewModeCropComputed;
use Drupal\view_mode_crop\ViewModeCropData;

/**
 * Plugin implementation of the 'image' field type which supports cropping.
 *
 * Implemented using view_mode_crop_field_info_alter().
 */
class ViewModeCropImageItem extends ImageItem {

  /**
   * {@inheritDoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $property_definitions = parent::propertyDefinitions($field_definition);

    $property_definitions['view_mode_crop'] = DataDefinition::create('any')
      ->setLabel(new TranslatableMarkup('View mode crop info'))
      ->setComputed(TRUE)
      ->setClass(ViewModeCropComputed::class);

    return $property_definitions;
  }

  /**
   * Get the crop data.
   *
   * @return \Drupal\view_mode_crop\ViewModeCropData[]
   *   The crop data.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function getViewModeCropData(): array {
    $view_mode_crop = ViewModeCropData::createFromJsonStringArray($this->get('view_mode_crop')
      ->getValue());

    return CropImageHelper::getCropData($this->getEntity(), $this->getFieldDefinition()
      ->getName(), $view_mode_crop);
  }

}
