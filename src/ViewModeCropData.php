<?php

namespace Drupal\view_mode_crop;

/**
 * Contains cropping data for a view mode.
 */
class ViewModeCropData {

  /**
   * The view mode id.
   *
   * @var string
   */
  public $id;

  /**
   * The view mode label.
   *
   * @var string
   */
  public $label;

  /**
   * Crop X.
   *
   * @var int|null
   */
  public $x;

  /**
   * Crop Y.
   *
   * @var int|null
   */
  public $y;

  /**
   * Crop width.
   *
   * @var int|null
   */
  public $w;

  /**
   * Crop height.
   *
   * @var int|null
   */
  public $h;

  /**
   * Constructor.
   *
   * @param string $id
   *   The view mode id.
   * @param string $label
   *   The view mode label.
   * @param int|null $x
   *   Crop X.
   * @param int|null $y
   *   Crop Y.
   * @param int|null $w
   *   Crop width.
   * @param int|null $h
   *   Crop height.
   */
  public function __construct(string $id, string $label, int $x = NULL, int $y = NULL, int $w = NULL, int $h = NULL) {
    $this->id = $id;
    $this->label = $label;
    $this->x = $x;
    $this->y = $y;
    $this->w = $w;
    $this->h = $h;
  }

  /**
   * Create an instance from an array.
   *
   * @param array $data
   *   The array.
   *
   * @return static
   *   the instance
   */
  public static function createFromArray(array $data) {
    return new static(
      $data['id'],
      $data['label'],
      $data['x'],
      $data['y'],
      $data['w'],
      $data['h']
    );
  }

  /**
   * Create an array  of instances from the given JSON string.
   *
   * @param string|null $json
   *   The JSON string.
   *
   * @return \Drupal\view_mode_crop\ViewModeCropData[]
   *   The array of instances.
   */
  public static function createFromJsonStringArray(string $json = NULL): array {
    if ($json === NULL) {
      return [];
    }
    $json = json_decode($json, TRUE);
    if (empty($json)) {
      return [];
    }
    $result = [];
    foreach ($json as $key => $value) {
      $result[$key] = self::createFromArray($value);
    }
    return $result;
  }

}
