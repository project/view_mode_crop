<?php

namespace Drupal\view_mode_crop;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * The view mode crop UI builder.
 */
class ViewModeCropUiBuilder {

  use StringTranslationTrait;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The currently active request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, RequestStack $request_stack, FormBuilderInterface $form_builder) {
    $this->entityTypeManager = $entity_type_manager;
    $this->request = $request_stack->getCurrentRequest();
    $this->formBuilder = $form_builder;
  }

  /**
   * Build the cropper ui.
   *
   * @param array $triggering_element
   *   The element that triggers the build.
   *
   * @return array
   *   the ui
   */
  public function buildUi(array $triggering_element): array {
    $state = $triggering_element['#view_mode_crop_state'];
    $build = [
      '#theme' => 'view_mode_crop_wrapper',
      '#attributes' => [
        'id' => 'view-mode-crop-wrapper',
        'data-entity-type-id' => $state->entityTypeId,
        'data-entity-id' => $state->id,
        'data-field-name' => $state->fieldName,
        'data-delta' => $state->delta,
        'data-triggering-element-name' => $triggering_element['#name'],
      ],
      'content' => [
        '#type' => 'container',
      ],
      '#attached' => [
        'library' => [
          'view_mode_crop/ui',
        ],
      ],
    ];

    return $build;
  }

  /**
   * Get the dialog options.
   *
   * @return array
   *   The options.
   */
  public static function dialogOptions(ViewModeCropState $state): array {

    return [
      'dialogClass' => 'view-mode-crop-widget-modal',
      'title' => t('Crop image'),
      'height' => '90%',
      'width' => '90%',
      'buttons' => [
        'all' => [
          'class' => 'button-link',
          'text' => t('Select all'),
          'id' => 'view-mode-crop-' . $state->entityTypeId . '-' . $state->id . '-' . $state->fieldName . '-' . $state->delta . '-button-all',
        ],
        'reset' => [
          'class' => 'button-link',
          'text' => t('Reset'),
          'id' => 'view-mode-crop-' . $state->entityTypeId . '-' . $state->id . '-' . $state->fieldName . '-' . $state->delta . '-button-reset',
        ],
        'save_close' => [
          'class' => 'button--primary',
          'text' => t('Apply and close'),
          'id' => 'view-mode-crop-' . $state->entityTypeId . '-' . $state->id . '-' . $state->fieldName . '-' . $state->delta . '-button-save-close',
        ],
        'cancel' => [
          'class' => 'button--danger',
          'text' => t('Cancel'),
          'id' => 'view-mode-crop-' . $state->entityTypeId . '-' . $state->id . '-' . $state->fieldName . '-' . $state->delta . '-button-cancel',
        ],
      ],
    ];
  }

}
