CONTENTS OF THIS FILE
---------------------

* Introduction
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------

The View Mode Crop module allows content editors to crop images to use in
a website. The availability of the cropping function is determined by the
view modes defined for the entity the image or media entity reference fields
are attached to. This means you can create distinct cropped images for each
view mode.

Cropping of image fields:
You can enable cropping for image fields by selecting the "Image (cropped)"
formatter for the field.

Cropping of media image fields:
Cropping of media-based image fields allows you to do cropping at the entity
level instead of the media level, which means that distinct cropped variants
of the same image media can be used in different entities.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/view_mode_crop

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/view_mode_crop


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

You can enable cropping for image and/or media image fields using the entity
view mode configuration screen.


MAINTAINERS
-----------

Current maintainers:
 * Ruud Simons (Ruuds) - https://www.drupal.org/u/ruuds
